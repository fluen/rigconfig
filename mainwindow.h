#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUrl>
#include <QJsonObject>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void setEnvironment(QString env);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_fluenButton_clicked();

private:
    Ui::MainWindow *ui;
    QString environment;
    QUrl adminURL;
    QJsonObject config;

};

#endif // MAINWINDOW_H
