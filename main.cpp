#include "mainwindow.h"
#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    if (argc > 1) {
        w.setEnvironment(argv[1]);
    } else {
        w.setEnvironment("prod");
    }
    w.show();

    return a.exec();
}
