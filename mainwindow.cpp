#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QDir>
#include <QJsonObject>
#include <QJsonDocument>
#include <QProcess>




MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->webview->hide();
    MainWindow::setFixedHeight(310);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setEnvironment(QString env)
{
    this->environment = env;
    if (env == "prod") {
        this->adminURL = QUrl("https://love.fluen.org/register");

        QJsonObject config
        {
            {"FluenURL", "love.fluen.org"},
            {"SexyServerURL", "https://stats.fluen.org"},
            {"RabbitURL","amqp://guest:guest@rabbit.fluen.org:5672/"},
            {"FluenPort","80"},
            {"Email",""},
            {"FluenKey",""},
            {"RigName",""}
        };
        this->config = config;


    } else {
        this->adminURL = QUrl("http://volvo.fluen.org/register");

        QJsonObject config
        {
            {"FluenURL", "volvo.fluen.org"},
            {"SexyServerURL", "volvo.fluen.org:8080"},
            {"RabbitURL","amqp://guest:guest@volvo.fluen.org:5672/"},
            {"FluenPort","80"},
            {"Email",""},
            {"FluenKey",""},
            {"RigName",""}
        };
        this->config = config;

    }
}


void MainWindow::on_pushButton_clicked()
{


    QFile file("/etc/fluen/fluen.conf");
    if (!file.open(QIODevice::Text | QIODevice::Truncate | QIODevice::WriteOnly)) {
        QMessageBox msgBox;
        msgBox.setText("Error writing /etc/fluen/fluen.conf file.\n" + file.errorString());
        msgBox.exec();
        return;
    }

    this->config["Email"]=ui->emailEdit->text();
    this->config["FluenKey"]=ui->fluenKeyEdit->text();
    this->config["RigName"]=ui->rigName->text();

    QJsonDocument doc(this->config);
    QByteArray bytes = doc.toJson();

    QTextStream out(&file);
    out << bytes;
    file.close();

    QMessageBox msgBox;

    if (QProcess::execute("/usr/bin/fluen rr") != 0) {
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("There was an error registering your Rig. Try again, check if you entered correct data.");
    } else {
        msgBox.setIcon(QMessageBox::Information);
        msgBox.setText("Your settings have been saved. Thank you.");

    }
    msgBox.exec();

}


void MainWindow::on_fluenButton_clicked()
{
    if (ui->webview->isVisible()) {
        ui->webview->hide();
        MainWindow::setFixedHeight(310);

    } else {
        ui->webview->load(this->adminURL);
        ui->webview->show();
        MainWindow::setFixedHeight(800);

    }
}
